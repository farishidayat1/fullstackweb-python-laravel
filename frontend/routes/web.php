<?php

use App\Http\Controllers\CustomerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CustomerController::class, 'index'])->name('customer');

Route::prefix('customer')->group(function() {
    Route::get('/create', [CustomerController::class, 'create'])->name('customer.create');
    Route::get('/show', [CustomerController::class, 'show'])->name('customer.show');
    Route::get('/edit', [CustomerController::class, 'edit'])->name('customer.edit');
});

// Route::get('/', function () {
//     return view('welcome');
// });
