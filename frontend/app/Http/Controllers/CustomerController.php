<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {
        return view('app.admin.customer.index');
    }

    public function create() 
    {
        return view('app.admin.customer.create');
    }

    public function edit() 
    {
        return view('app.admin.customer.edit');
    }

    public function show() 
    {
        return view('app.admin.customer.show');
    }
}
