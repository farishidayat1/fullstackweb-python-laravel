@extends('app.admin.layout')
<style>
    .btn-primary {
        margin-bottom: 20px;
    }
</style>
@section('content')  
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-flex justify-content-between">
            <h1 class="h3 mb-2 text-gray-800">Customers</h1>
            <a class="btn btn-primary btn-icon-split" data-toggle="modal" data-target="#createModal">
                <span class="text">Create</span>
            </a>            
        </div>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            {{-- @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif
            @if(session()->has('error_message'))
                <div class="alert alert-error">
                    {{ session()->get('error_message') }}
                </div>
            @endif --}}
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">List of Customers</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Customer Name</th>
                                <th>Instagram Username</th>
                                <th>Favorites Outfit Color</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <!-- Create Modal-->
    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Customer</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="customer_name" class="form-label">Customer Name</label>
                        <input type="text" class="form-control" name="customer_name" id="customer_name">
                    </div>
                    <div class="mb-3">
                        <label for="username_ig" class="form-label">Instagram Username</label>
                        <input type="text" class="form-control" name="username_ig" id="username_ig">
                    </div>
                    <div class="mb-3">
                        <label for="favorites_outfit_color" class="form-label">Favorites Outfit Color</label>
                        <input type="text" class="form-control" name="favorites_outfit_color" id="favorites_outfit_color">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" onclick="createCustomer()">Save</a>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var table = null

        $(document).ready(function(){
            renderTable()
        });    

        function confirmationDelete(id) {
            const alert = confirm('Are you sure to delete this transaksi ?')
            if(alert) {
                $.ajax({
                    url: "http://localhost:5000/api/customers/delete/"+id,
                    method: "DELETE",
                    contentType: 'application/json',
                    success: function(response) {
                        renderTable()
                    },
                    error: function(error) {
                        console.log(error)
                    }
                });    
            }
        }

        function renderTable() {
            $.ajax({
                url: "http://localhost:5000/api/customers",
                method: "GET",
                success: function(data) {
                    table = $('#dataTable').dataTable({
                        bDestroy: true,
                        aaData: data,
                        columns: [
                            { "data": 'id' },
                            { "data": "customer_name" },
                            { "data": "username_ig" },
                            { "data": "favorites_outfit_color" },
                            { "data" : 'id',
                                render : function(data, type, row) {
                                    return `<a class="btn btn-danger" onclick="confirmationDelete(${data})">
                                                <i class="fas fa-trash"></i>
                                            </a>`
                                }    
                            },
                        ]
                    })
                },
                error: function(error) {
                    console.log(error)
                }
            });    
        }

        function createCustomer() {
            const customer_name = $('#customer_name').val()
            const username_ig = $('#username_ig').val()
            const favorites_outfit_color = $('#favorites_outfit_color').val()

            $.ajax({
                url: "http://localhost:5000/api/customers/store",
                method: "POST", 
                data: JSON.stringify({
                    customer_name: customer_name,
                    username_ig: username_ig,
                    favorites_outfit_color: favorites_outfit_color
                }),
                contentType: 'application/json',
                success: function(response) {
                    renderTable()
                    $('#createModal').modal('toggle');
                },
                error: function(error) {
                    console.log(error)
                }
            });    
        }
    </script>
@endsection