# Setup Backend
1. Install python
2. pip install Flask
3. pip install db-sqlite3
4. python api.py

# Setup Frontend
1. Install Composer and Php 
2. Run Composer install 
3. Duplicate file .env.example and rename to .env
4. Run php artisan migrate 
5. Run php artisan serve