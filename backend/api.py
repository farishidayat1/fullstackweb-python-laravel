#!/usr/bin/python
import sqlite3
from flask import Flask, request, jsonify
from flask_cors import CORS


def connect_to_db():
    conn = sqlite3.connect('database.db')
    return conn


def create_db_table():
    try:
        conn = connect_to_db()
        # conn.execute('''DROP TABLE customers''')
        conn.execute('''
            CREATE TABLE customers (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                customer_name TEXT NOT NULL,
                username_ig TEXT NOT NULL,
                favorites_outfit_color TEXT NOT NULL
            );
        ''')

        conn.commit()
        print("Customer table created successfully")
    except:
        pass
        print("Customer table creation failed - Maybe table")
    finally:
        conn.close()


def insert(customer):
    inserted_customer = {}
    try:
        conn = connect_to_db()
        cur = conn.cursor()
        cur.execute("INSERT INTO customers (customer_name, username_ig, favorites_outfit_color) VALUES (?, ?, ?)", (customer['customer_name'], customer['username_ig'], customer['favorites_outfit_color']) )
        conn.commit()
        inserted_customer = get_customer(cur.lastrowid)
    except:
        conn().rollback()

    finally:
        conn.close()

    return inserted_customer


def get_customers():
    customers = []
    try:
        conn = connect_to_db()
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute("SELECT * FROM customers")
        rows = cur.fetchall()

        # convert row objects to dictionary
        for i in rows:
            customer = {}
            customer["id"] = i["id"]
            customer["customer_name"] = i["customer_name"]
            customer["username_ig"] = i["username_ig"]
            customer["favorites_outfit_color"] = i["favorites_outfit_color"]
            customers.append(customer)

    except:
        customers = []

    return customers


def get_customer(id):
    customer = {}
    try:
        conn = connect_to_db()
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute("SELECT * FROM customers WHERE id = ?", (id,))
        row = cur.fetchone()

        # convert row object to dictionary
        customer["id"] = row["id"]
        customer["customer_name"] = row["customer_name"]
        customer["username_ig"] = row["username_ig"]
        customer["favorites_outfit_color"] = row["favorites_outfit_color"]
    except:
        customer = {}

    return customer


def update(customer):
    updated_customer = {}
    try:
        conn = connect_to_db()
        cur = conn.cursor()
        cur.execute("UPDATE customers SET customer_name = ?, username_ig = ?, favorites_outfit_color = ? WHERE id = ?", (customer["customer_name"], customer["username_ig"], customer["favorites_outfit_color"], customer["id"],))
        conn.commit()
        updated_customer = get_customer(customer["id"])

    except:
        conn.rollback()
        updated_customer = {}
    finally:
        conn.close()

    return updated_customer


def delete(id):
    message = {}
    try:
        conn = connect_to_db()
        conn.execute("DELETE from customers WHERE id = ?", (id,))
        conn.commit()
        message["status"] = "Customer deleted successfully"
    except:
        conn.rollback()
        message["status"] = "Cannot delete Customer"
    finally:
        conn.close()

    return message


create_db_table()


app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})

@app.route('/api/customers', methods=['GET'])
def api_get_customers():
    return jsonify(get_customers())

@app.route('/api/customers/<id>', methods=['GET'])
def api_get_customer(id):
    return jsonify(get_customer(id))

@app.route('/api/customers/store',  methods = ['POST'])
def api_create_customer():
    customer = request.get_json()
    return jsonify(insert(customer))

@app.route('/api/customers/update',  methods = ['PUT'])
def api_update():
    customer = request.get_json()
    return jsonify(update(customer))

@app.route('/api/customers/delete/<id>',  methods = ['DELETE'])
def api_delete(id):
    return jsonify(delete(id))


if __name__ == "__main__":
    #app.debug = True
    #app.run(debug=True)
    app.run()